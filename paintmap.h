//Rainbow - a colour name matcher
//    Copyright (C) 2017  Nevyn McAnferny

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>
#ifndef PAINTMAP_H
#define PAINTMAP_H

#include <QString>

class PaintMap
{
    QString oldName;
    QString newName;

public:
    PaintMap(QString Old, QString New);
    QString GetNewName();
    QString GetOldName();
};

#endif // PAINTMAP_H
