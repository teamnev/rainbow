//Rainbow - a colour name matcher
//    Copyright (C) 2017  Nevyn McAnferny

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QCoreApplication::setOrganizationName("ShaTek");
    QCoreApplication::setApplicationName("Rainbow");
    QSettings settings;
    QString fileName = settings.value("filename").toString();
    LoadData(fileName);

    QList<PaintMap>::iterator i;
    for (i = paints.begin(); i != paints.end(); ++i)
    {
        ui->newComboBox->addItem( (*i).GetNewName() );
        ui->oldComboBox->addItem( (*i).GetOldName() );
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

MainWindow::LoadData(QString fileName)
{
    QString val;
    QFile file;
    file.setFileName(fileName);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    val = file.readAll();
    file.close();

    QJsonDocument jsonResponse = QJsonDocument::fromJson(val.toUtf8());
    QJsonObject jsonObject = jsonResponse.object();
    QJsonArray jsonArray = jsonObject["paints"].toArray();

    foreach (const QJsonValue & value, jsonArray) {
        QJsonObject obj = value.toObject();
        QString tmpold = obj["oldColour"].toString();
        QString tmpnew = obj["newColour"].toString();
        paints << PaintMap(tmpold,tmpnew);
    }

    return 0;
}

void MainWindow::on_actionE_xit_triggered()
{
    QApplication::quit();
}

void MainWindow::on_oldComboBox_currentIndexChanged(int index)
{
    ui->newComboBox->setCurrentIndex(index);
}

void MainWindow::on_newComboBox_currentIndexChanged(int index)
{
    ui->oldComboBox->setCurrentIndex(index);
}
