# Rainbow

Rainbow is a very small GUI app to translate oldnames to newnames. This is based on my need to work out what the colours I have are now called in the Citadel paint range.

## TODO

* This needs an installer. At this point it only runs from within the QTCreator, but it does everything I need it to.
* This probably needs some tests.
* Datafile definition needs adding to this readme.
